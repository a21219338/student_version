package com.example.student_version.pushNotice;

import com.example.student_version.pushNotice.Sender;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiSERVICE {
@Headers({
        "Content-Type:application/json," +
                "Authorization:key=AAAAxzL6wTQ:APA91bGaRavhD_B22KU49Eslf2JST8Qnyt_OFSyxbCPK7sF13Pir37aCV1Ccp-BrFCH0BnVW5iyrYvYfxs47FcGHkRl4yQHZxhK9v1Pma16inLYyLN3UDzfO5BUhS4I5rAXGFFaZivTs"
})
    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);


}
