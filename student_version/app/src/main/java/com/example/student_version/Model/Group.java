package com.example.student_version.Model;

public class Group {
    private String CreateBy;
    private String TimeStamp;
    private String groupIcon;
    private String groupId;
    private String groupTitle;

    public Group(String createBy, String timeStamp, String groupIcon, String groupId, String groupTitle) {
        CreateBy = createBy;
        TimeStamp = timeStamp;
        this.groupIcon = groupIcon;
        this.groupId = groupId;
        this.groupTitle = groupTitle;
    }

    public Group() {
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }
}
