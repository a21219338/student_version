package com.example.student_version.Map;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionsJSONParser {
    /** 接收一個JSONObject並返回一個列表的列表，包含經緯度 */
    public List<List<HashMap<String, String>>> parse_point(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        JSONArray j3=new JSONArray();





        try {

            jRoutes = jObject.getJSONArray("routes");
            System.out.println("jRoutes:"+jRoutes);

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                System.out.println("jLegs:"+jLegs);


                List path = new ArrayList<HashMap<String, String>>();
                List html = new ArrayList<HashMap<String, String>>();
     
                List location = new ArrayList<HashMap<String, String>>();
                List TIMES = new ArrayList<HashMap<String, String>>();
                List information= new ArrayList<HashMap<String, String>>();
                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    HashMap<String, String> hpss = new HashMap<>();
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");




                    System.out.println("jSteps:"+jSteps);
                   JSONObject duration=((JSONObject) jLegs.get(j)).getJSONObject("duration");
                    System.out.println("duration"+duration);
                    String time = duration.getString("text");
                    hpss.put("text",time);
                    TIMES.add(hpss);
                    /** Traversing all steps */
                    int count=0;
                    int n=0;
                    for (int k = 0; k < jSteps.length(); k++) {

                        if (count<jSteps.length() && k%2==0){
                            JSONObject jsonLeg  = jSteps.getJSONObject(count);//0
                            j3.put(n,jsonLeg.optJSONArray("steps"));
                            System.out.println("step2"+j3);

                            count+=2;
                            n++;
                        }

                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        /** 取出start_location */
                        HashMap<String, String> hps = new HashMap<>();
                        Double start_lat,start_lon ;
                        start_lat = (double)  ((JSONObject) ((JSONObject) jSteps.get(k)).get("start_location")).get("lat");
                        start_lon=(double)  ((JSONObject) ((JSONObject) jSteps.get(k)).get("start_location")).get("lng");
                        Log.d("start", String.valueOf(start_lat));
                        String parse_lat= Double. toString(start_lat);
                        String parse_lng= Double.toString(start_lon);
                        hps.put("start_location_lat",parse_lat);
                        hps.put("start_location_lng",parse_lng);
                        location.add(hps);



                        /** 取出html */
                        HashMap<String, String> hp = new HashMap<String, String>();
                        String html_str = "";
                        html_str = (String) ((JSONObject) jSteps.get(k)).get("html_instructions");
                        hp.put("html_str",html_str);
                        html.add(hp);




                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }


                    };



                    Log.d("LOCATION",location.toString());
                    Log.d("DT",information.toString());
                    routes.add(path); /**對映到MainActivity IF i = 0*/
                    routes.add(html); /**對映到MainActivity IF i = 1*/
                    routes.add(location);/**對映到MainActivity IF i = 2*/
                    routes.add(TIMES);/**對映到MainActivity IF i = 3*/
                    routes.add(information);/**對映到MainActivity IF i = 4*/

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }

        return routes;
    }

    /**目前沒用到**/
    public List<HashMap<String, String>> parse_html(JSONObject jObject) {

        List<HashMap<String, String>> routes = new ArrayList<HashMap<String, String>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        try {
            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String html_str= "";
                        html_str = (String)((JSONObject)jSteps.get(k)).get("html_instructions");
                        // hm.put("html_str",html_str);

                    }
                    //routes.add(path);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }

        return routes;

    }

    /**
     * 解碼折線點的方法
     * */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}
