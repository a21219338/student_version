package com.example.student_version.Subject.exbook;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.Adapter.PlaceAdpter;
import com.example.student_version.Model.ExbookData;
import com.example.student_version.Adapter.ExbookDataAdapter;
import com.example.student_version.Model.PlaceData;
import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;

import java.util.ArrayList;

public class exbookFragment extends Fragment {
    DatabaseHelpler mydb;
    String content="";
    PlaceData[]placeData=new PlaceData[100];
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_exbook,container, false);
        RecyclerView recyclerView=root.findViewById(R.id.recyleview);
        recyclerView.setHasFixedSize(true); //新增item 不影響recycle view尺寸
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mydb=new DatabaseHelpler(getActivity());
        Cursor cursor=mydb.getSearchData();
        while (cursor.moveToNext()){
            content=cursor.getString(1);
        }
        cursor=mydb.selectLessonData(content);

        while(cursor.moveToNext()){
            content=cursor.getString(9);
        }
        String[] all=content.split("<br />");
        int count=0;
        PlaceData pls[]=new PlaceData[all.length];
        for (int i=0;i<all.length;i++){
            Log.d("place",all[i]);

            pls[i] = new PlaceData(all[i],R.drawable.ic_action_place);

        }
        PlaceAdpter adapter=new PlaceAdpter(pls,getActivity());
        recyclerView.setAdapter(adapter);



        return root;
    }
}
