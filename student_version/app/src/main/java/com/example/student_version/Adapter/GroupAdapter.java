package com.example.student_version.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.student_version.notice.GroupChattActivity;
import com.example.student_version.Model.Group;
import com.example.student_version.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {
private Context mContext;
private List<Group> mGroup;

public GroupAdapter(Context mContext, List<Group> mGroup) {
        this.mContext = mContext;
        this.mGroup = mGroup;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.group_item,parent,false);
        return  new GroupAdapter.ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Group group =mGroup.get(position);
        holder.groupname.setText(group.getGroupTitle());

        loadLastMessage(group,holder);
        if (group.getGroupIcon().equals("")){
        holder.prfile_img.setImageResource(R.mipmap.ic_launcher);
        }else {
        Glide.with(mContext).load(group.getGroupIcon()).into(holder.prfile_img);

        }
        holder.itemView.setOnClickListener(new View.OnClickListener(){
             @Override
            public void onClick(View v) {
                 Log.d("name",holder.groupname.getText().toString());
                Intent intent =new Intent(mContext, GroupChattActivity.class);
                intent.putExtra("groupid",group.getGroupId());
                mContext.startActivity(intent);
             }
        });


}

private void loadLastMessage(Group group, ViewHolder holder) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Groups");
        ref.child(group.getGroupId()).child("Messages").limitToLast(1)
        .addValueEventListener(new ValueEventListener(){
@Override
public void onDataChange(@NonNull DataSnapshot snapshot) {
        for (DataSnapshot dataSnapshot:snapshot.getChildren()){
        String mes=""+dataSnapshot.child("message").getValue();
        String timestamp=""+dataSnapshot.child("timestamp").getValue();
        String senderss=""+dataSnapshot.child("sender").getValue();
        String type=""+dataSnapshot.child("type").getValue();


        Calendar c=Calendar.getInstance(Locale.TAIWAN);
        c.setTimeInMillis(Long.parseLong(timestamp));

        String dateTime= DateFormat.format("dd/MM hh:mm aa",c).toString();
        holder.tv_time.setText(dateTime);

        if (type.equals("image")){
        holder.message.setText("see photo");
        }else {
        holder.message.setText(mes);
        }
//                        從 group 中的sender 尋找user
//                            DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Users");
//                            reference.orderByChild("id").equalTo(senderss)
//                                    .addValueEventListener(new ValueEventListener() {
//                                        @Override
//                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
//                                            for(DataSnapshot dataSnapshot:snapshot.getChildren()){
//                                                String NAME=""+dataSnapshot.child("username").getValue();
//
//                                                holder.sender.setText(NAME);
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onCancelled(@NonNull DatabaseError error) {
//
//                                        }
//                                    });


        }

        }

@Override
public void onCancelled(@NonNull DatabaseError error) {

        }
        });
        }


@Override
public int getItemCount() {
        return mGroup.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    public TextView groupname,sender,message,tv_time;
    public ImageView prfile_img;
    public ViewHolder(View itemView){
        super(itemView);
        groupname=itemView.findViewById(R.id.titles);
        prfile_img=itemView.findViewById(R.id.profile_image);
        message=itemView.findViewById(R.id.message);
        tv_time=itemView.findViewById(R.id.time);

    }

}
}

