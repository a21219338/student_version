package com.example.student_version.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.student_version.R;

public class EdProfile extends AppCompatActivity {
    TextView store;
    ImageView goback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ed_profile);
        store=findViewById(R.id.store);
        goback=findViewById(R.id.goback);
        Intent intent=getIntent();
        String email=intent.getStringExtra("email");
        String classes=intent.getStringExtra("classes");

        goback.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EdProfile.this.finish();
            }
        });

        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(EdProfile.this,ProfileActivity.class);
                startActivity(intent);
            }
        });
    }
}