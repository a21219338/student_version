package com.example.student_version;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;

public class RegesterActivity extends AppCompatActivity {
    MaterialEditText username,email,password;
    Button bt_rs;
    FirebaseAuth auth;
    DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regester);
        username=findViewById(R.id.username);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        bt_rs=findViewById(R.id.res);

        auth=FirebaseAuth.getInstance();
        bt_rs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(username.getText().toString()) ||TextUtils.isEmpty(email.getText().toString())
                        || TextUtils.isEmpty(password.getText().toString())){
                    Toast.makeText(RegesterActivity.this,"all fill are required ",Toast.LENGTH_LONG).show();

                }else  if(password.getText().length()<6){
                    Toast.makeText(RegesterActivity.this,"password length at least 6 words ",Toast.LENGTH_LONG).show();
                }else {
                    register(username.getText().toString(),email.getText().toString(),
                            password.getText().toString());
                }

            }
        });

    }
    private  void  register(String username, String email, String password){
        auth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser firebaseUser=auth.getCurrentUser();
                            String userid=firebaseUser.getUid();

                            reference= FirebaseDatabase.getInstance().getReference("Student").child(userid);

                            HashMap<String,String> hahMap=new HashMap<>();

                            hahMap.put("id",userid);
                            hahMap.put("username",username);
                            hahMap.put("imageUrl","default");
                            hahMap.put("Classes","default");
                            hahMap.put("gender","default");
                            hahMap.put("birthday","default");
                            hahMap.put("telphone","default");
                            hahMap.put("address","default");
                            hahMap.put("semester","default");
                            hahMap.put("student_ID","default");
                            hahMap.put("ID_number","default");
                            hahMap.put("guardian","default");
                            hahMap.put("guardian_phone","default");
                            hahMap.put("email",email);


                            reference.setValue(hahMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                   // Toast.makeText(RegesterActivity.this, "success", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(RegesterActivity.this,MainActivity.class));

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(RegesterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else {
                            Toast.makeText(RegesterActivity.this,"YOU CAN'T REGISTER",Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

}