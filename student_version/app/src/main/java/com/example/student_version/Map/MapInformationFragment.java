package com.example.student_version.Map;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.student_version.Adapter.MapAdapter;
import com.example.student_version.Adapter.MapInfromationAdapter;
import com.example.student_version.R;

import java.util.Arrays;
import java.util.Collections;

public class MapInformationFragment extends Fragment {
    String splitePlace[]=new String[10];
    String splitAddress[]=new String[20];
    RecyclerView recyclerView;
    MapInfromationAdapter mapIntoationAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view =inflater.inflate(R.layout.fragment_map_information, container, false);
        Bundle bundle = getArguments();
        recyclerView=view.findViewById(R.id.recycle_view);

        if(bundle != null)
        {
            splitePlace = bundle.getStringArray("splitePlace");
            splitAddress=bundle.getStringArray("splitAddress");

        }
        recyclerView=view.findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mapIntoationAdapter = new MapInfromationAdapter(getContext(),splitePlace,splitAddress);
        recyclerView.setAdapter(mapIntoationAdapter);
        ItemTouchHelper itemTouchHelper =new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


        return  view;
    }
    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.DOWN|
            ItemTouchHelper.UP | ItemTouchHelper.START |ItemTouchHelper.END,0) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            int fromPostion=viewHolder.getAdapterPosition();
            int toPostion=target.getAdapterPosition();
            Collections.swap(Arrays.asList(splitePlace),fromPostion,toPostion);
            Collections.swap(Arrays.asList(splitAddress),fromPostion,toPostion);
            recyclerView.getAdapter().notifyItemMoved(fromPostion,toPostion);
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

        }
    };
}