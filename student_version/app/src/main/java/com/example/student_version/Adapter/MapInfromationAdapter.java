package com.example.student_version.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.R;



public class MapInfromationAdapter extends RecyclerView.Adapter<MapInfromationAdapter.ViewHolder> {
private Context mContext;

 String[] mUser =new String [10];
String[]mAddress=new String [10];

public MapInfromationAdapter(Context mContext, String[] mUser,String[] mplace) {
        this.mContext = mContext;
        this.mUser = mUser;
        this.mAddress = mplace;

        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.map_item,parent,false);

        return  new MapInfromationAdapter.ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



        holder.username.setText(mUser[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        Bundle bundle=new Bundle();
        bundle.putString("place",mUser[position]);
        bundle.putString("address",mAddress[position]);
        Navigation.findNavController(v).navigate(R.id.action_map_infromation_fragment_to_navigation_bottom_direction,bundle);


        }
 });
}

@Override
public int getItemCount() {
        return mUser.length;
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    public TextView username;

    public ViewHolder(View itemView){
        super(itemView);
        username=itemView.findViewById(R.id.name);


    }

}
}

