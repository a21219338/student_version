package com.example.student_version.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.student_version.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    TextView edProfile,email,classes,name;
    ImageView goback , changePicture;
    private CircleImageView profileImageView;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private Uri imageUri;
    private String myuri="";
    private StorageTask uploadTask;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        edProfile=findViewById(R.id.et_profile);
        changePicture=findViewById(R.id.changePicture);
        email=findViewById(R.id.email);
        goback=findViewById(R.id.goback);
        classes=findViewById(R.id.classes);
        name =findViewById(R.id.name);
        Intent intent=getIntent();
        name.setText(intent.getStringExtra("name"));
        changePicture.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivity.this,EditPicture.class);
            }
        });
        goback.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ProfileActivity.this.finish();
            }
        });
        edProfile.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this,EdProfile.class);
                intent.putExtra("email",email.getText());
                intent.putExtra("classes",classes.getText());
                startActivity(intent);
            }
        });
    }
}