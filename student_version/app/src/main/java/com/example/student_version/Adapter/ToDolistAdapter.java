package com.example.student_version.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.R;

import java.util.ArrayList;
import java.util.List;

public class ToDolistAdapter extends RecyclerView.Adapter<ToDolistAdapter.ViewHolder> {
private Context mContext;
private List<String> mUser=new ArrayList<>();


public ToDolistAdapter(Context mContext, List<String> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.todolist_item,parent,false);

        return  new ToDolistAdapter.ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.username.setText(mUser.get(position));

}

@Override
public int getItemCount() {
        return mUser.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    public TextView username;
    public ImageView prfile_img;
    public ViewHolder(View itemView){
        super(itemView);
        username=itemView.findViewById(R.id.tv_content);

    }

}
}
