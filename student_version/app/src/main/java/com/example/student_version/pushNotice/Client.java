package com.example.student_version.pushNotice;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
//由 RESTful Client 向 Server 發出請求

public class Client {
    private static  Retrofit retrofit =null;

    public static Retrofit getRetrofit(String url) {
        if (retrofit == null){

            retrofit =new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }
}
