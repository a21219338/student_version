package com.example.student_version.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.Model.ExbookData;
import com.example.student_version.Model.PlaceData;
import com.example.student_version.R;

import java.util.Random;


public class PlaceAdpter extends RecyclerView.Adapter<PlaceAdpter.ViewHolder> {
    PlaceData[]data;
    Context context;
    public PlaceAdpter(PlaceData[] versions, Context context){
        this.data =versions;
    this.context=context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cardview_place,parent,false);
       ViewHolder viewHolder = new PlaceAdpter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final PlaceData placeData=data[position];
        int images[]={R.drawable.place,R.drawable.place1};
        Random r = new Random();
        int index=r.nextInt(2);
        holder.place.setText(placeData.getPlace());
        holder.imageView.setImageResource(images[index]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,placeData.getPlace(),Toast.LENGTH_LONG).show();
                Bundle bundle = new Bundle();
                bundle.putString("place",placeData.getPlace());

                Navigation.findNavController(v).navigate(R.id.action_navigation_bottom_exbook_to_navigation_bottom_exbookDetail,bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView place;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            imageView = itemView.findViewById(R.id.imageview);
            place = itemView.findViewById(R.id.eventplace);


        }
    }
}

