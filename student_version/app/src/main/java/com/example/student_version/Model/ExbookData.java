package com.example.student_version.Model;

public class ExbookData {


    private String chapter,SUBJECT,VERSION,id1;
    private Integer img;

    public ExbookData() {
    }

    public ExbookData(String chapter, String SUBJECT, String VERSION, String id1, Integer img) {
        this.chapter = chapter;
        this.SUBJECT = SUBJECT;
        this.VERSION = VERSION;
        this.id1 = id1;
        this.img = img;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getSUBJECT() {
        return SUBJECT;
    }

    public void setSUBJECT(String SUBJECT) {
        this.SUBJECT = SUBJECT;
    }

    public String getVERSION() {
        return VERSION;
    }

    public void setVERSION(String VERSION) {
        this.VERSION = VERSION;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public Integer getImg() {
        return img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }
}
