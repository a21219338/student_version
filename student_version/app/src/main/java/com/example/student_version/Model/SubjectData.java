package com.example.student_version.Model;

public class SubjectData {
    private String chapterName;
    private String subject;
    private Integer img;
    public  SubjectData(String chapterName,String subject,Integer image){
        this.chapterName=chapterName;
        this.subject=subject;
        this.img=image;
    }

    public String getChapterName() {
        return chapterName;
    }

    public String getSubject() {
        return subject;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setImg(Integer img) {
        this.img = img;
    }

    public Integer getImg() {
        return img;
    }
}
