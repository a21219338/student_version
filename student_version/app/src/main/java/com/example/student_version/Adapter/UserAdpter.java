package com.example.student_version.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.student_version.Model.User;
import com.example.student_version.R;
import com.example.student_version.notice.MessageActivity;

import java.util.List;

public class UserAdpter extends RecyclerView.Adapter<UserAdpter.ViewHolder> {
    private Context mContext;
    private List<User> mUser;

    public UserAdpter(Context mContext, List<User> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_item,parent,false);
        return  new UserAdpter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = mUser.get(position);
        holder.username.setText(user.getusername());
        if(user.getImageUrl().equals("default")){
            holder.prfile_img.setImageResource(R.mipmap.ic_launcher);
        }else {
            Glide.with(mContext).load(user.getImageUrl()).into(holder.prfile_img);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(mContext, MessageActivity.class);

                intent.putExtra("userId",user.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView username;
        public ImageView prfile_img;
        public ViewHolder(View itemView){
            super(itemView);
            username=itemView.findViewById(R.id.usernames);
            prfile_img=itemView.findViewById(R.id.profile_image);

        }

    }
}

