package com.example.student_version.pushNotice;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.util.Random;

public class OreoAndAboveNotifiction  extends ContextWrapper {
    private static final String ID="some_id";
    private static final String NAME="FirebaseAPP";
    private  NotificationManager notificationManager;
    public  OreoAndAboveNotifiction(Context base){
        super(base);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
           createNotificationChannel();
        }
    }
    @TargetApi(Build.VERSION_CODES.O)
    private  void createNotificationChannel(){
        NotificationChannel notificationChannel =new NotificationChannel(ID,
                NAME,NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setDescription("Channel");
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.setVibrationPattern(new long[]{0,1000,500,1000});
        notificationChannel.enableLights(true);
        getNotificationManager().createNotificationChannel(notificationChannel);
    }
    public NotificationManager getNotificationManager(){
        if (notificationManager == null){
            notificationManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        } return notificationManager;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotifications(String title, String body, PendingIntent pendingIntent
    , Uri sourceUri, String icon){
        return new Notification.Builder(getApplicationContext(),ID)
                .setContentIntent(pendingIntent)
                .setContentTitle(title)
                .setContentText(body)
                .setSound(sourceUri)
                .setAutoCancel(true)
                .setSmallIcon(Integer.parseInt(icon));
    }

}

