package com.example.student_version.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.R;
import com.example.student_version.Model.ExbookData;

public class ExbookDataAdapter extends RecyclerView.Adapter<ExbookDataAdapter.ViewHolder> {
    ExbookData[]data;
    Context context;
    public ExbookDataAdapter(ExbookData []versions,Context context){
        this.data=versions;
        this.context=context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cardview_exbook,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ExbookData exbookData=data[position];
        holder.version.setText(exbookData.getSUBJECT()+" "+exbookData.getVERSION());
        holder.chpater.setText(exbookData.getChapter());
        holder.imageView.setImageResource(exbookData.getImg());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,exbookData.getChapter(),Toast.LENGTH_LONG).show();
                Bundle bundle = new Bundle();
                bundle.putString("id1",exbookData.getId1());
                bundle.putString("chapter",exbookData.getChapter());
                Navigation.findNavController(v).navigate(R.id.action_navigation_bottom_exbookPlaceDetail_to_navigation_bottom_exbookDetail,bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView version,chpater;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            imageView = itemView.findViewById(R.id.imageview);
            version = itemView.findViewById(R.id.version);
            chpater=itemView.findViewById(R.id.chapter);

        }
    }
}
