package com.example.student_version.notice;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.student_version.Adapter.GroupAdapter;
import com.example.student_version.Model.Group;
import com.example.student_version.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class GroupFragment extends Fragment {
    private List<Group> mGroup;
    private FirebaseAuth firebaseAuth;
    private GroupAdapter groupAdapter;
    RecyclerView recyclerView;
    SearchView searchView;
    public GroupFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_group, container, false);
        mGroup=new ArrayList<>();
        firebaseAuth=FirebaseAuth.getInstance();
        recyclerView =view.findViewById(R.id.recycle_view);
        searchView=view.findViewById(R.id.serarch);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!TextUtils.isEmpty(query.trim())){
                    searchGroup(query);
                }else{
                    readGroup();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!TextUtils.isEmpty(newText.trim())){
                    searchGroup(newText);
                }else{
                    readGroup();
                }
                return false;
            }
        });
        readGroup();

        FloatingActionButton fab =view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), GroupActivity.class));
            }
        });


        return  view;
    }

    private void searchGroup(String query) {
        FirebaseUser fUser=FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref=FirebaseDatabase.getInstance().getReference("Groups");
        ref.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mGroup.clear();
                for(DataSnapshot ds:snapshot.getChildren()){
                    if (ds.child("Member").child(firebaseAuth.getUid()).exists()){

                        if(ds.child("groupTitle").toString().toLowerCase().contains(query.toLowerCase())){
                            Group groups=ds.getValue(Group.class);
                            mGroup.add(groups);
                        }
                    }
                }
                groupAdapter =new GroupAdapter(getContext(),mGroup);
                recyclerView.setAdapter(groupAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void readGroup() {

        DatabaseReference df= FirebaseDatabase.getInstance().getReference("Groups");
        df.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mGroup.clear();
                for(DataSnapshot ds:snapshot.getChildren()){
                    Group groups=ds.getValue(Group.class);
                    assert groups !=null;
                    assert  firebaseAuth!=null;
                    if(ds.child("Member").child(firebaseAuth.getUid()).exists()){
                        Log.d("con","ok");
                        mGroup.add(groups);
                    }
                    groupAdapter=new GroupAdapter(getContext(),mGroup);
                    recyclerView.setAdapter(groupAdapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


}