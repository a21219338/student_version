package com.example.student_version.notice;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.Adapter.UserAdpter;
import com.example.student_version.MainActivity;
import com.example.student_version.Model.User;
import com.example.student_version.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupActivity extends AppCompatActivity {
    private  static final  int CAMERA_REQUEST_CODE=100;
    private  static final  int STORAGE_REQUEST_CODE=200;

    private static final int IMAGE_PICK_CAMERA_CODE=300;
    private static final int IMAGE_PICK_GALLERY_CODE=400;

    private String[]cameraPermission;
    private String[]storagePermission;
    private RecyclerView recyclerView;
    private Uri image_uri=null;

    private FirebaseAuth firebaseAuth;
    EditText title;
    CircleImageView groupIcon;
    ImageButton addmember;
    boolean condition=false;
    private List<User> mUser;
    private UserAdpter userAdapter;
    TextView tv_groupName;
    String id="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        recyclerView =findViewById(R.id.groupmemberlist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(GroupActivity.this));




        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        TextView tv_build=findViewById(R.id.build);
        tv_groupName=findViewById(R.id.tv_groupName);
        Bundle bundle = this.getIntent().getExtras();
        if(bundle != null){
            condition = bundle.getBoolean("condition");
            id=""+bundle.getString("id");
        }



        if (condition == true){
          memberlist();

        }
        tv_build.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startCreateGroup();
            }
        });
        groupIcon =findViewById(R.id.groupIcon);
        title =findViewById(R.id.tv_groupName);
        addmember=findViewById(R.id.insert_member);
        cameraPermission=new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};
        storagePermission=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        firebaseAuth=FirebaseAuth.getInstance();
        groupIcon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showImagePickDailog();
            }
        });
        addmember.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GroupActivity.this,AddMemberActivity.class));
            }
        });

    }

    private void startCreateGroup() {
        String groupName =tv_groupName.getText().toString().trim();
        if (TextUtils.isEmpty(groupName)){
            Toast.makeText(this, "請輸入群組名稱", Toast.LENGTH_SHORT).show();
            return;
        }
        String timestamp=""+System.currentTimeMillis();
        if (image_uri ==null){
            createGroup(""+timestamp,""+groupName,"");
        }else{
            String fileNameAndPath ="Group_Imgs/"+"image"+timestamp;

            StorageReference storageReference = FirebaseStorage.getInstance().getReference(fileNameAndPath);
            storageReference.putFile(image_uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> p_uriTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!p_uriTask.isSuccessful());
                            Uri p_downloadUri=p_uriTask.getResult();
                            if (p_uriTask.isSuccessful()){
                                createGroup(
                                        ""+timestamp,
                                        ""+groupName,
                                        ""+p_downloadUri

                                );
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(GroupActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    private void createGroup(String timestamp,String title,String icon) {
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("groupId",""+timestamp);
        hashMap.put("groupTitle",""+title);
        hashMap.put("groupIcon",""+icon);
        hashMap.put("TimeStamp",""+timestamp);
        hashMap.put("CreateBy",firebaseAuth.getUid());
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Groups");
        reference.child(timestamp).setValue(hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Toast.makeText(GroupActivity.this,"create group success ..",Toast.LENGTH_LONG).show();

                HashMap<String,String >hashMap1 =new HashMap<>();
                hashMap1.put("MemberId",firebaseAuth.getUid());
                hashMap1.put("timestamp",timestamp);
                DatabaseReference reference1 = FirebaseDatabase.getInstance().getReference("Groups");
                reference1.child(timestamp).child("Member").child(firebaseAuth.getUid())
                        .setValue(hashMap1)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                Toast.makeText(GroupActivity.this,"group creaating ..",Toast.LENGTH_LONG).show();
                                DatabaseReference references=FirebaseDatabase.getInstance().getReference("SelctMember");
                                HashMap<String,String> hashMaps=new HashMap<>();
                                hashMaps.put("timestamp",timestamp);
                                references.child(id).child("people").addValueEventListener(new ValueEventListener(){
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        for(DataSnapshot ds:snapshot.getChildren()){
                                            String ids=""+ds.child("selectuserId").getValue();
                                            hashMaps.put("MemberId",ids);
                                            reference1.child(timestamp).child("Member").child(ids)
                                                    .setValue(hashMaps)
                                                    .addOnSuccessListener(new OnSuccessListener(){
                                                        @Override
                                                        public void onSuccess(Object o) {

                                                        }
                                                    }).addOnFailureListener(new OnFailureListener(){
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.e("error",e.getMessage());
                                                }
                                            });
                                        }

                                    }



                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.w("error",e.getMessage());
                        Toast.makeText(GroupActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
                isOk(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(GroupActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
            }
        });





    }

    private void isOk(boolean b) {
        if (b ==true){
            final Intent localIntent = new Intent(GroupActivity.this, MainActivity.class);
            localIntent.putExtra("destination","group");
            Timer timer = new Timer();
            TimerTask tast = new TimerTask() {
                @Override
                public void run() {
                    startActivity(localIntent);

                }
            };
            timer.schedule(tast,  3000);

        }

    }

    private void memberlist() {
        mUser=new ArrayList<>();
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference("SelctMember");
        reference.child(id).child("people").addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mUser.clear();
                for (DataSnapshot ds:snapshot.getChildren()){
                    String ids=""+ds.child("selectuserId").getValue();

                    DatabaseReference df=FirebaseDatabase.getInstance().getReference("Student");
                    df.orderByChild("id").equalTo(ids).addValueEventListener(new ValueEventListener(){

                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            for(DataSnapshot dataSnapshot:snapshot.getChildren()){
                                User user=dataSnapshot.getValue(User.class);
                                Log.d("IDS",user.getusername());
                                mUser.add(user);
                            }


                        }


                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }

                userAdapter=new UserAdpter(GroupActivity.this,mUser);
                recyclerView.setAdapter(userAdapter);
                Toast.makeText(GroupActivity.this, "success", Toast.LENGTH_SHORT).show();

            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    private void showImagePickDailog() {
        String []options = {"Camera","Gallery"};
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("PICK Image:")
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which ==0){
                            if (!checkCameraPermissions()){
                                requestCameraPermission();
                            }else {
                                pickCamera();
                            }
                        }else{
                            if (!checkStoragePermission()){
                                requestStoragePermissions();
                            }else {
                                pickGallery();
                            }
                        }
                    }
                }).show();
    }
    private void  pickGallery(){
        Intent intent =new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent,IMAGE_PICK_GALLERY_CODE);
    }
    private void pickCamera(){
        ContentValues cv =new ContentValues();
        cv.put(MediaStore.Images.Media.TITLE,"group image icon title");
        image_uri =getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,cv);
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(intent,IMAGE_PICK_CAMERA_CODE);
    }
    private boolean checkStoragePermission(){
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result;
    }
    private void requestStoragePermissions(){
        ActivityCompat.requestPermissions(this,storagePermission,STORAGE_REQUEST_CODE);
    }
    private  boolean checkCameraPermissions(){
        boolean result =ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) ==(PackageManager.PERMISSION_GRANTED);
        boolean result1=ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) ==(PackageManager.PERMISSION_GRANTED);
        return result &&result1 ;
    }
    private void requestCameraPermission(){
        ActivityCompat.requestPermissions(this,cameraPermission,CAMERA_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CAMERA_REQUEST_CODE:{
                if(grantResults.length>0){
                    boolean cameraAccepted =grantResults[0]==PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted =grantResults[1]==PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted && storageAccepted){
                        pickCamera();
                    }else{
                        Toast.makeText(GroupActivity.this,"camera && storage permission are required",Toast.LENGTH_LONG).show();

                    }
                }
            }

            break;
            case STORAGE_REQUEST_CODE:{
                if(grantResults.length>0){
                    boolean storageAccepted =grantResults[0]==PackageManager.PERMISSION_GRANTED;

                    if (storageAccepted){
                        pickGallery();
                    }else{
                        Toast.makeText(GroupActivity.this," storage permission are required",Toast.LENGTH_LONG).show();

                    }
                }
            }
            break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode ==IMAGE_PICK_GALLERY_CODE){
                image_uri=data.getData();
                groupIcon.setImageURI(image_uri);
            }else if (requestCode ==IMAGE_PICK_CAMERA_CODE){

                groupIcon.setImageURI(image_uri);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
