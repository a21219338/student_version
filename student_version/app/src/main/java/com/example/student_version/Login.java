package com.example.student_version;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    final static String TAG="Login";
    Button bt_log,bt_register;
    EditText account,password;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bt_log=findViewById(R.id.bt_login);
        account=findViewById(R.id.editTextAccount);
        password=findViewById(R.id.editTextPassword);
        bt_register=findViewById(R.id.bt_register);

        firebaseAuth=FirebaseAuth.getInstance();
        bt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login.this,RegesterActivity.class);
                startActivity(intent);
            }
        });
        bt_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String accounts=account.getText().toString();
                String passwords=password.getText().toString();
                if (TextUtils.isEmpty(accounts) || TextUtils.isEmpty(passwords)){
                    Toast.makeText(Login.this, "不能空白", Toast.LENGTH_SHORT).show();
                }else{
                    firebaseAuth.signInWithEmailAndPassword(accounts,passwords)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        startActivity(new Intent(Login.this,MainActivity.class));


                                        finish();
                                    }else{
                                        Toast.makeText(Login.this, "帳號或密碼有誤", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });
                }


            }
        });


    }
}