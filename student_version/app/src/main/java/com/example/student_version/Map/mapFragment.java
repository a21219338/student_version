package com.example.student_version.Map;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.student_version.Adapter.MapAdapter;
import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;

import java.util.ArrayList;
import java.util.List;


public class mapFragment extends Fragment {

    DatabaseHelpler mydb;
    List<String> name;
    List<String> place;
    List<String> address;
    RecyclerView recyclerView;
    MapAdapter mapAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_map,container,false);
        mydb=new DatabaseHelpler(getActivity());
        recyclerView = root.findViewById(R.id.recycle_view);
        name=new ArrayList<>(); place=new ArrayList<>(); address=new ArrayList<>();
        Cursor cursor=mydb.getLessonData();
        while(cursor.moveToNext()){
            name.add(cursor.getString(1));


        }
        recyclerView=root.findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mapAdapter = new MapAdapter(getContext(),name);
        recyclerView.setAdapter(mapAdapter);



        return root;
    }
}