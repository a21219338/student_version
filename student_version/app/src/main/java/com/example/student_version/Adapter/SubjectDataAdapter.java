package com.example.student_version.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.R;
import com.example.student_version.Model.SubjectData;

import java.util.Random;

public class SubjectDataAdapter extends RecyclerView.Adapter<SubjectDataAdapter.ViewHolder> {



    SubjectData[] data;
    Context context;
    public SubjectDataAdapter(SubjectData[]versions,Context context){
        this.data=versions;
        this.context=context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cardview_subject,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final SubjectData subjectDatalist=data[position];
      //  Log.d("dd", subjectDatalist.getChapterName());
        holder.title.setText(subjectDatalist.getChapterName());
        holder.imageView.setImageResource(subjectDatalist.getImg());
        holder.version.setText(subjectDatalist.getSubject());
        int[] images = {R.drawable.cv1,R.drawable.cv4,R.drawable.cv5};
        Random rand = new Random();

        int index = rand.nextInt(3);
        Drawable drawable =context.getResources().getDrawable(images[index]);
        holder.layout.setBackground(drawable);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(v).navigate(R.id.action_navigation_dashboard_to_navigation_exdetail);
                Toast.makeText(context, subjectDatalist.getChapterName(), Toast.LENGTH_SHORT).show();
                Bundle bundle=new Bundle();
                bundle.putString("event",subjectDatalist.getChapterName());

                Navigation.findNavController(v).navigate(R.id.action_navigation_subject_to_navigation_bottom_release,bundle);

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length;
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView title,version;
        LinearLayout layout;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            imageView = itemView.findViewById(R.id.imageview);
            title = itemView.findViewById(R.id.eventName);
            version=itemView.findViewById(R.id.subject);
            layout=itemView.findViewById(R.id.bag_linear);


        }
    }
}
