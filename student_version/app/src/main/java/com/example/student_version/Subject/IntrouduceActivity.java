package com.example.student_version.Subject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.student_version.R;

public class IntrouduceActivity extends AppCompatActivity {
    String place=null,introuce="";
    TextView tv_in;
    ImageButton ig_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introuduce);
        tv_in=findViewById(R.id.tv_in);
        ig_back=findViewById(R.id.goback);
        ig_back.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Bundle bundle=getIntent().getExtras();
        if (bundle!= null){
            place =bundle.getString("place");
            introuce=bundle.getString("introuduce");

        }
        tv_in.setText(introuce);

    }
}