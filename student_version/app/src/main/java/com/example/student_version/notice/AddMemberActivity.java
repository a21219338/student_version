package com.example.student_version.notice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.student_version.Adapter.AddMemberAdapter;
import com.example.student_version.Adapter.UserAdpter;
import com.example.student_version.Model.User;
import com.example.student_version.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddMemberActivity extends AppCompatActivity {
    private AddMemberAdapter userAdapter;
    private List<User> mUser;
    private RecyclerView recyclerView;
    private FirebaseAuth firebaseAuth;
    private TextView tv_build;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        Toolbar toolbar = findViewById(R.id.toolbar);
         setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        tv_build=findViewById(R.id.build);

        recyclerView =findViewById(R.id.recycleview);
        firebaseAuth=FirebaseAuth.getInstance();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(AddMemberActivity.this));
        mUser=new ArrayList<>();
        tv_build.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(AddMemberActivity.this,GroupActivity.class);
                Bundle bundle = new Bundle();
                boolean condition=true;
                bundle.putBoolean("condition",condition);
                bundle.putString("id",firebaseAuth.getUid());
                i.putExtras(bundle);
                startActivity(i);


            }
        });
        readUser();

    }
    private void readUser() {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Student");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mUser.clear();
                for (DataSnapshot datasnapshot :snapshot.getChildren()){
                    User user=datasnapshot.getValue(User.class);
                    if (!firebaseAuth.getUid().equals(user.getId())){
                        mUser.add(user);
                    }
                }
             userAdapter =new AddMemberAdapter(AddMemberActivity.this,mUser,firebaseAuth.getUid());
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}