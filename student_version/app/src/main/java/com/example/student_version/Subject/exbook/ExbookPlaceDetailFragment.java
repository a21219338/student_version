package com.example.student_version.Subject.exbook;

import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.student_version.Adapter.ExbookDataAdapter;
import com.example.student_version.Adapter.PlaceAdpter;
import com.example.student_version.Model.ExbookData;
import com.example.student_version.Model.PlaceData;
import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ExbookPlaceDetailFragment extends Fragment {
    DatabaseHelpler mydb;

    private String place ="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root= inflater.inflate(R.layout.fragment_exbook_place_detail, container, false);
        RecyclerView recyclerView=root.findViewById(R.id.recyleview);
        recyclerView.setHasFixedSize(true); //新增item 不影響recycle view尺寸
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mydb=new DatabaseHelpler(getActivity());
        Bundle bundle =getArguments();
        if (bundle !=null) {
            place = bundle.getString("place");

        }
        Log.d("placess", place);
        String id1[] = new String[100];
        String[] addexbook = new String[100];
        String[] exbook = new String[100];
        int count = 0;
        Cursor cursor = mydb.selectMainTextData("安平古堡");
        while (cursor.moveToNext()) {
            Log.d("id1", cursor.getString(5));
            id1[count] = cursor.getString(1);
            addexbook[count] = cursor.getString(25);
            exbook[count] = cursor.getString(26);
            count++;

        }
//            Log.d("counts", String.valueOf(count));
        ArrayList<String> anss= new ArrayList<>();
        int counts = 0;
        for (int i = 0; i < count; i++) {
            String ans[] = addexbook[i].split(",");
            for (String c : ans) {
                anss.add(counts,c);
//                    Log.d("arr" + counts + ": ", anss[counts]);
                counts++;
            }
        }
        anss.removeAll(Arrays.asList("", null));
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < anss.size(); i++) {
            if (!list.contains(anss.get(i)) ) {
                list.add(anss.get(i));
            }
        }
        list.removeAll(Arrays.asList("", null));
        Log.d("SIZE", String.valueOf(list.size()));
        String[] Versin=new String[30];String[] subject=new String[30];String[] chapterName=new String[30];
        for (int i=0;i<list.size();i++){
            Log.d("LISTSS",list.get(i));
            cursor =mydb.selectMainTextId(list.get(i));
            while (cursor.moveToNext()){
                Versin[i]=cursor.getString(2);
                subject[i]=cursor.getString(4);
                chapterName[i]="CH "+cursor.getString(8)+" "+cursor.getString(9);
            }
        }
        ExbookData pls[]=new ExbookData[list.size()];
        for (int i=0;i<list.size();i++){


            pls[i] = new ExbookData(chapterName[i],subject[i],Versin[i],list.get(i),R.drawable.ic_action_place);

        }
        ExbookDataAdapter adapter=new ExbookDataAdapter(pls,getContext());
        recyclerView.setAdapter(adapter);


        return root;

    }
}