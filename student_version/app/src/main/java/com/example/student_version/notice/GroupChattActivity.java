package com.example.student_version.notice;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student_version.Adapter.GroupChatAdapter;
import com.example.student_version.Model.GroupChat;
import com.example.student_version.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GroupChattActivity extends AppCompatActivity {
    String groupId=null;
    private static final int CAMERA_REQUEST_CODE=200;
    private static final int STORAGE_REQUEST_CODE=400;

    private static final int IMAGE_PICK_GALLERY_CODE=1000;
    private static final int IMAGE_PICK_CAMERA_CODE=2000;

    private String[]cameraPermission;
    private String[]storagePermission;

    private Uri image_uri=null;
    ImageButton bt_attach,bt_send,info,back;
    EditText message;
    private FirebaseAuth firebaseAuth;
    private RecyclerView rv_groupChat;
    private GroupChatAdapter groupChatAdapter;
    private List<GroupChat>groupChatList;
    TextView tv_groupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chatt);
        back=findViewById(R.id.back);
        info = findViewById(R.id.info);
        tv_groupName=findViewById(R.id.groupname);
        message=findViewById(R.id.messages);
        bt_attach=findViewById(R.id.attach);
        bt_send=findViewById(R.id.sendMessage);
        rv_groupChat=findViewById(R.id.groutChattRecycleView);
        rv_groupChat.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        rv_groupChat.setLayoutManager(linearLayoutManager);
        firebaseAuth=FirebaseAuth.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        groupId=intent.getStringExtra("groupid");
        Log.d("groupid",groupId);


        cameraPermission=new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        storagePermission=new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        loadGroupMessages();
        bt_send.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String messages =message.getText().toString().trim();
                if (TextUtils.isEmpty(messages)){
                    Toast.makeText(GroupChattActivity.this,"CAN'T SEND EMPTY MESSAGE",Toast.LENGTH_LONG).show();

                }else{
                    sendMessage(messages);
                }
            }
        });

        loadGroupInformation();
        bt_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageImportDialog();
            }
        });
        info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

            }
        });
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void loadGroupInformation() {
        DatabaseReference databaseReference =FirebaseDatabase.getInstance().getReference("Groups");
        databaseReference.orderByChild("groupId").equalTo(groupId)
                .addValueEventListener(new ValueEventListener(){
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot ds : snapshot.getChildren()){
                            String groupTitles=""+ds.child("groupTitle").getValue();
                            tv_groupName.setText(groupTitles);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void loadGroupMessages() {
        groupChatList=new ArrayList<>();
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference("Groups");
        ref.child(groupId).child("Messages")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        groupChatList.clear();
                        for(DataSnapshot dataSnapshot:snapshot.getChildren()){
                            GroupChat gt =dataSnapshot.getValue(GroupChat.class);
                            groupChatList.add(gt);

                        }
                        groupChatAdapter =  new GroupChatAdapter(GroupChattActivity.this,groupChatList);
                        rv_groupChat.setAdapter( groupChatAdapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
    private void sendMessage(String messages) {
        String timestamp=""+System.currentTimeMillis();
        HashMap<String,Object> hashMap=new HashMap<>();
        hashMap.put("sender",""+firebaseAuth.getUid());
        hashMap.put("message",""+messages);
        hashMap.put("timestamp",""+timestamp);
        hashMap.put("type",""+"text");

        DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Groups");
        reference.child(groupId).child("Messages").child(timestamp)
                .setValue(hashMap)
                .addOnSuccessListener(new OnSuccessListener(){

                    @Override
                    public void onSuccess(Object o) {
                        message.setText("");

                    }
                }).addOnFailureListener(new OnFailureListener(){

            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(GroupChattActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void showImageImportDialog() {
        String options[]={"Camera","Gallery"};
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("Pick Image")
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which ==0){
                            if (!checkCameraPermission()){
                                requestCameraPermission();
                            }else{
                                PickCamera();
                            }
                        }else {
                            if (!checkStoragePermission()){
                                requestStoragePermission();
                            }else{
                                pickGallery();
                            }
                        }
                    }
                }).show();
    }
    private  void pickGallery(){
        Intent intent =new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent,IMAGE_PICK_GALLERY_CODE);
    }
    private void PickCamera(){
        ContentValues contentValues=new ContentValues();
        contentValues.put(MediaStore.Images.Media.TITLE,"GroupImageTITLE");
        contentValues.put(MediaStore.Images.Media.DESCRIPTION,"GroupImageDESCRIPTION");
        image_uri=getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,contentValues);
        Intent intent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,image_uri);
        startActivityForResult(intent,IMAGE_PICK_CAMERA_CODE);

    }
    private void requestStoragePermission(){
        ActivityCompat.requestPermissions(this,storagePermission,STORAGE_REQUEST_CODE);
    }
    private boolean checkStoragePermission(){
        boolean result = ContextCompat.checkSelfPermission(
                this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);

        return result;
    }
    private void requestCameraPermission(){
        ActivityCompat.requestPermissions(this,cameraPermission,CAMERA_REQUEST_CODE);

    }
    private boolean checkCameraPermission(){
        boolean result = ContextCompat.checkSelfPermission(
                this,Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);
        boolean result1 = ContextCompat.checkSelfPermission(
                this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);

        return result && result1;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode ==RESULT_OK){
            if (requestCode == IMAGE_PICK_GALLERY_CODE){
                image_uri = data.getData();
                sendImageMessage();
            }
            if (requestCode ==IMAGE_PICK_CAMERA_CODE){

            }
        }
    }

    private void sendImageMessage() {
        ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Please wait");
        pd.setMessage("sending image ...");
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        String filenamePath="chatImages/"+""+System.currentTimeMillis();
        StorageReference storageReference = FirebaseStorage.getInstance().getReference(filenamePath);
        storageReference.putFile(image_uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> p_uriTask =taskSnapshot.getStorage().getDownloadUrl();
                        while (!p_uriTask.isSuccessful());
                        Uri p_download =p_uriTask.getResult();
                        if (p_uriTask.isSuccessful()){
                            String timeStamp =""+System.currentTimeMillis();
                            HashMap<String,Object>hashMap=new HashMap<>();
                            hashMap.put("sender",""+firebaseAuth.getUid());
                            hashMap.put("message",""+image_uri);
                            hashMap.put("timestamp",""+timeStamp);
                            hashMap.put("type",""+"image");
                            DatabaseReference reference =FirebaseDatabase.getInstance().getReference("Groups");
                            reference.child(groupId).child("Messages").child(timeStamp).setValue(hashMap)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            pd.dismiss();
                                            message.setText("");
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    pd.dismiss();
                                    Toast.makeText(GroupChattActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(GroupChattActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                if (grantResults.length >0){
                    boolean cameraAccepted =grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted =grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted && writeStorageAccepted){
                        PickCamera();
                    }else {
                        Toast.makeText(GroupChattActivity.this,"CAMERA & STORAGE PERMISSION ARE REQUIRED",Toast.LENGTH_SHORT).show();
                    }
                }break;

            case STORAGE_REQUEST_CODE:
                if (grantResults.length >0){
                    boolean writeStorageAccepted =grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if ( writeStorageAccepted){
                        pickGallery();
                    }else {
                        Toast.makeText(GroupChattActivity.this," STORAGE PERMISSION is REQUIRED",Toast.LENGTH_SHORT).show();
                    }
                }break;

        }
    }



}