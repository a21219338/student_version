package com.example.student_version.Map;

import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class DirectionFragment extends Fragment {
    String place="",address = "";
    DatabaseHelpler mydb;
    private ArrayList<LatLng> dest = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_direction, container, false);
        Bundle bundle =getArguments();
        mydb=new DatabaseHelpler(getActivity());
        mydb.deleteSearchData();

        if (bundle !=null){
            place= bundle.getString("place");
            address= bundle.getString("address");
            mydb.inserSearchData(address);
        }else{
            Cursor cursor =mydb.getSearchData();
            while(cursor.moveToNext()){

                address =cursor.getString(1);
            }

        }
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocationName(address, 1);
            if (addressList.size() > 0) {
                Address addr = addressList.get(0);
                dest = new ArrayList<LatLng>();
                dest.add(new LatLng(addr.getLatitude(), addr.getLongitude()));


            }

        } catch (Exception e) {
            e.printStackTrace();
        }


                   Uri dst=Uri.parse("google.navigation:q="+dest.get(0).latitude+","+dest.get(0).longitude+"&mode=b");

                    Intent intent=new Intent(Intent.ACTION_VIEW,
                            dst);
                    intent.setPackage("com.google.android.apps.maps");

                    if (intent.resolveActivity(getActivity().getPackageManager() )!=null){
                        startActivity(intent);
                    }




        return view;
    }
}