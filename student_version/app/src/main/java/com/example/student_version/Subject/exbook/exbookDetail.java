package com.example.student_version.Subject.exbook;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.student_version.R;
import com.example.student_version.Subject.IntrouduceActivity;
import com.example.student_version.database.DatabaseHelpler;

public class exbookDetail extends Fragment {
    DatabaseHelpler mydb;String chapter="",id="",place="";
    TextView tv_goal,tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv_event,tv_mon,more;
    LinearLayout two,three,four,five,six,seven;
    String NICKNAME = null ,mon = null
            , tue = null ,wed = null ,thur = null, fri = null, sat = null, sun = null, season = null, introduction = null,events="";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_exbook_detail,container,false);
        mydb=new DatabaseHelpler(getActivity());
        tv_goal =root.findViewById(R.id.goal);
        tv2 =root.findViewById(R.id.tv2);
        tv1 =root.findViewById(R.id.tv1);
        tv3 =root.findViewById(R.id.tv3);
        tv4 =root.findViewById(R.id.tv4);
        tv6 =root.findViewById(R.id.tv6);
        tv5 =root.findViewById(R.id.tv5);
        tv7 =root.findViewById(R.id.tv7);
        tv_event=root.findViewById(R.id.event);
        two=root.findViewById(R.id.tues);
        three=root.findViewById(R.id.wed);
        four=root.findViewById(R.id.thur);
        five=root.findViewById(R.id.fri);
        six=root.findViewById(R.id.sat);
        seven=root.findViewById(R.id.sun);
        tv_mon=root.findViewById(R.id.tv_mon);
        more =root.findViewById(R.id.tv_more);
        more.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("place",place);
                bundle.putString("introuduce",introduction);
                Log.d("INS",introduction);
                Intent intent=new Intent(getContext(), IntrouduceActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        Bundle bundle = getArguments();
        if(bundle != null)
        {

            place =bundle.getString("place");


        }
        Cursor cursor =mydb.selectMainTextData(place);


        while (cursor.moveToNext()){
            //12 - 20  22
            id = cursor.getString(1);
            events=cursor.getString(11);
             NICKNAME=cursor.getString(12);
             mon=cursor.getString(13);
             tue=cursor.getString(14);
             wed=cursor.getString(15);
             thur=cursor.getString(16);
             fri=cursor.getString(17);
             sat=cursor.getString(18);
             sun=cursor.getString(19);
             season=cursor.getString(20);
             introduction=cursor.getString(22);
             Log.d("MON,",mon);
             Log.d("ID0,",id);
        }
        tv_event.setText(events);
        tv_goal.setText(introduction);
        cursor =mydb.selectTimeListData(mon);
        String contentM=null;
        while (cursor.moveToNext()){
            contentM =cursor.getString(2);
        }
        tv1.setText(contentM);
        cursor =mydb.selectTimeListData(tue);
        while (cursor.moveToNext()){
            contentM=cursor.getString(2);
        }
        tv2.setText(contentM);
        cursor =mydb.selectTimeListData(wed);
       while (cursor.moveToNext()){
           contentM=cursor.getString(2);
        }
        tv3.setText(contentM);
        cursor =mydb.selectTimeListData(thur);
        while (cursor.moveToNext()){
            contentM=cursor.getString(2);
        }
        tv4.setText(contentM);
        cursor =mydb.selectTimeListData(fri);
        while (cursor.moveToNext()){
            contentM=cursor.getString(2);
        tv5.setText(contentM);
        }
        cursor =mydb.selectTimeListData(sat);
        while (cursor.moveToNext()){
            contentM=cursor.getString(2);
        }
        tv6.setText(contentM);
        cursor =mydb.selectTimeListData(sun);
        while (cursor.moveToNext()){
            contentM=cursor.getString(2);
        }
        tv7.setText(contentM);
        if (tv1.getText().equals(tv2.getText()) || tv1.getText().equals(tv3.getText()) || tv1.getText().equals(tv4.getText())
        || tv1.getText().equals(tv5.getText()) || tv1.getText().equals(tv6.getText()) || tv1.getText().equals(tv7.getText())){
            two.setVisibility(View.INVISIBLE); three.setVisibility(View.INVISIBLE); four.setVisibility(View.INVISIBLE);
            five.setVisibility(View.INVISIBLE); six.setVisibility(View.INVISIBLE); seven.setVisibility(View.INVISIBLE);
            tv_mon.setVisibility(View.INVISIBLE);
        }

        return root;
    }
}
