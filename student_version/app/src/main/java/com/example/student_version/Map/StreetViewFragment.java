package com.example.student_version.Map;

import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;
import com.google.android.gms.maps.model.StreetViewSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class StreetViewFragment extends Fragment  implements OnStreetViewPanoramaReadyCallback {

    DatabaseHelpler mydb;
    String address="";

    private ArrayList<LatLng> dest = new ArrayList<>();
    private StreetViewPanorama mStreetViewPanorama;
    private ArrayList<LatLng> latLngArrayList=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_street, container, false);
        mydb=new DatabaseHelpler(getActivity());
        Cursor cursor =mydb.getSearchData();
        while(cursor.moveToNext()){
            address =cursor.getString(1);
        }
        Log.d("ADDRESS",address);
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocationName(address, 1);
            if (addressList.size() > 0) {
                Address addr = addressList.get(0);
                dest = new ArrayList<LatLng>();
                dest.add(new LatLng(addr.getLatitude(), addr.getLongitude()));


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        SupportStreetViewPanoramaFragment streetViewFragment = (SupportStreetViewPanoramaFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.streetviewpanorama);
        streetViewFragment.getStreetViewPanoramaAsync((OnStreetViewPanoramaReadyCallback) this);


        return view;
    }


    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        mStreetViewPanorama = streetViewPanorama;



            streetViewPanorama.setPosition(new LatLng(dest.get(0).latitude, dest.get(0).longitude), StreetViewSource.OUTDOOR);



        streetViewPanorama.animateTo(
                new StreetViewPanoramaCamera.Builder().
                        orientation(new StreetViewPanoramaOrientation(20, 20))
                        .zoom(streetViewPanorama.getPanoramaCamera().zoom)
                        .build(), 2000);

        streetViewPanorama.setOnStreetViewPanoramaChangeListener(panoramaChangeListener);

    }

    private StreetViewPanorama.OnStreetViewPanoramaChangeListener panoramaChangeListener =
            new StreetViewPanorama.OnStreetViewPanoramaChangeListener() {
                @Override
                public void onStreetViewPanoramaChange(
                        StreetViewPanoramaLocation streetViewPanoramaLocation) {

                    int i=0;
                    LatLng SS=new LatLng(streetViewPanoramaLocation.position.latitude,streetViewPanoramaLocation.position.longitude);
                    latLngArrayList.add(SS);
                    Log.d("第"+i+"個", String.valueOf(SS));
                    Log.d("LATARRAY",latLngArrayList.toString());
                    Toast.makeText(getContext(), "Lat: " + streetViewPanoramaLocation.position.latitude + " Lng: " + streetViewPanoramaLocation.position.longitude, Toast.LENGTH_SHORT).show();

                }
            };

}