package com.example.student_version.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;

import java.util.ArrayList;

import java.util.List;

public class MapAdapter  extends RecyclerView.Adapter<MapAdapter.ViewHolder> {
    private Context mContext;
    DatabaseHelpler mydb;
    List<String> name;
    List<String> place;
    List<String> address;
    private List<String> mUser =new ArrayList<>();

    public MapAdapter(Context mContext, List<String> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;

    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.map_item,parent,false);

        return  new MapAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



        holder.username.setText(mUser.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb=new DatabaseHelpler(mContext);
                Cursor cursor=mydb.getLessonData();
                name=new ArrayList<>(); place=new ArrayList<>(); address=new ArrayList<>();
                while(cursor.moveToNext()){

                    place.add(cursor.getString(9));
                    address.add(cursor.getString(10));

                }
                String[] splitePlace = place.get(position).split("<br />");
                String[] splitAddress=address.get(position).split("<br />");

                Bundle bundle=new Bundle();
                bundle.putStringArray("splitePlace",splitePlace);
                bundle.putStringArray("splitAddress",splitAddress);
                Navigation.findNavController(v).navigate(R.id.action_navigation_map_to_map_infromation_fragment,bundle);


            }
        });
    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView username;

        public ViewHolder(View itemView){
            super(itemView);
            username=itemView.findViewById(R.id.name);


        }

    }
}

