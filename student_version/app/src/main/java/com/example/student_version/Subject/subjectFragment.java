package com.example.student_version.Subject;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.Model.SubjectData;
import com.example.student_version.Adapter.SubjectDataAdapter;
import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;

public class subjectFragment extends Fragment {

    DatabaseHelpler mydb;
    SubjectData[]subjectData;
    String[] events=new String[10];
    String subject[]=new String[10];
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("SubjectFragment",":start");
        View root = inflater.inflate(R.layout.fragment_subject,container, false);
        RecyclerView recyclerView=root.findViewById(R.id.recyleview);
        recyclerView.setHasFixedSize(true); //新增item 不影響recycle view尺寸
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mydb=new DatabaseHelpler(getActivity());
        Cursor cursor=mydb.getEventData();
        int count = 0;
        while (cursor.moveToNext()){
            Log.d("event",cursor.getString(2));
            events[count]=cursor.getString(2);
            count++;
        }


    for (int i=0;i<count;i++){

        Cursor cursorS=mydb.selectLessonData(events[i]);
        while(cursor.moveToNext()){
            subject[i]=cursorS.getString(3);
        }



    }

            subjectData=new SubjectData[]{

                  new SubjectData(events[0], subject[0], R.drawable.ic_baseline_bookmark_24),
                    new SubjectData(events[1], subject[1], R.drawable.ic_baseline_bookmark_24),
            };

        if(count !=0){
            SubjectDataAdapter adapter=new SubjectDataAdapter(subjectData,getContext());
            recyclerView.setAdapter(adapter);
        }


        return root;
    }
}
