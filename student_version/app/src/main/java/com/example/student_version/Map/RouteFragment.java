package com.example.student_version.Map;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.student_version.R;
import com.example.student_version.database.DatabaseHelpler;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class RouteFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "RouteFragment";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private LocationManager locationManager;
    private WifiManager wifiManager;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LatLng mylocation;
    private GoogleMap mMap;
    private ArrayList<LatLng> origin;
    private ArrayList<LatLng> dest = new ArrayList<>();
    private ArrayList<String> html_str_array = null;
    private ArrayList<String> st_lat_array = null;
    private ArrayList<String> str_lng_array = null;

    DatabaseHelpler mydb;
   String address="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route, container, false);
        mydb=new DatabaseHelpler(getActivity());
        Cursor cursor =mydb.getSearchData();
        while(cursor.moveToNext()){
            address =cursor.getString(1);
        }
        Log.d("ADDRESS",address);
       getPermission();
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocationName(address, 1);
            if (addressList.size() > 0) {
                Address addr = addressList.get(0);
                dest = new ArrayList<LatLng>();
                dest.add(new LatLng(addr.getLatitude(), addr.getLongitude()));
                Log.d(TAG, "DEST" + dest.toString());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void getPermission() {
        String[] permissions = {FINE_LOCATION, COURSE_LOCATION}; //自訂權限
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(getActivity().WIFI_SERVICE);

        boolean LOCATION_setting = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER); //用來確認手機定位服務是否可使用
        if (LOCATION_setting) {
            //權限是否打開
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), COURSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "失敗");
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_REQUEST_CODE);
            } else {
                Log.e(TAG, "成功");
                init();

            }

        } else {
            Log.e(TAG, "未開啟定位");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        if (!wifiManager.isWifiEnabled()) {
            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            startActivity(intent);
        }
    }

    private void init() {
        Log.d(TAG, "init:地圖初始化");
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);  //地圖初始化

    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        getDeviceLocation();
    }

    private void getDeviceLocation() {
        Log.d(TAG,"getDeviceLocation:正在取得定位資訊");
        fusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(getActivity());
        try{
            //task 是在背景執行的一種非同步
            Task location=fusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){ //找到最新的定位資訊
                        Log.d(TAG,"OnCompleted:found location");
                        Location current_location=(Location) task.getResult(); //取得結果
                        mylocation=new LatLng(current_location.getLatitude(),current_location.getLongitude());
                        Log.d(TAG,"LAT:"+current_location);
                        if(current_location !=null){
                            moveCamera(new LatLng(current_location.getLatitude(),current_location.getLongitude()),
                                    DEFAULT_ZOOM,"my location");
                            destmarker();
                        }
                    }else {
                        Log.d(TAG,"current location is null");
                        Toast.makeText(getContext(),"unable to get current location ",Toast.LENGTH_SHORT).show();

                    }
                }
            });


        }catch (SecurityException e){
            Log.e(TAG,"getDevicesSecurityException"+e.getMessage());

        }

    }

    private void destmarker() {
        if(dest.size()>0 && dest!=null ){

            MarkerOptions markerOptions=new MarkerOptions();
            markerOptions.position(dest.get(0));
            mMap.addMarker(markerOptions);

            String url=  getRequestUrl(origin.get(0),dest.get(0));
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }
    }
    private String getRequestUrl(LatLng origin, LatLng dest) {
        Log.d(TAG,"getRequest");

        String str_org = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String language="language=zh-TW";
        String mode = "mode=transit";
        String transit="transit_mode=train|tram|subway";
        String key="key="+"AIzaSyAsEJbT97a-z8U6_y4QXZ5ayqGRm6MkVsk";
        String parm = str_org + "&" + str_dest + "&" + sensor + "&" +key+"&"+language+"&"+mode+"&"+transit;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parm;
        return url;

    }
    private String downloadUrl(String strUrl) throws IOException {
        Log.d(TAG,"讀取JSON檔");
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // 開啟連線
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // 讀取 strurl 資料
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d(String.valueOf(this), e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private void moveCamera(LatLng latLng, float Zoom, String title) {
        Log.d(TAG,"moveCameraTo"+latLng);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,Zoom));
        MarkerOptions options=new MarkerOptions()
                .position(latLng)
                .title(title);
        mMap.addMarker(options);

        if (title.equals("my location")){
            origin=new ArrayList<LatLng>();
            origin.add(latLng);
            Log.d(TAG,"origin:"+origin.toString());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to Mountain View
                    .zoom(20)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(30)// Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * 解析JSON格式
     **/
    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser =new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse_point(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            // point
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            // point

            // html

            // html

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                System.out.println(" main_result.size():" + result.size());
                /** 處理point */
                if (i == 0) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {

                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }
                }
                /** location*/
                if (i == 2) {
                    st_lat_array=new ArrayList<String>();
                    str_lng_array = new ArrayList<String>();
                    List<HashMap<String, String>> start_list = result.get(i);
                    Log.d("LIST",start_list.toString());
                    for (int k = 0; k < start_list.size(); k++) {
                        HashMap<String, String> html_map = start_list.get(k);
                        String start_location_lat = html_map.get("start_location_lat");
                        String startLocation_lng=html_map.get("start_location_lng");
                        Log.d("startLocation",start_location_lat);
                        st_lat_array.add(start_location_lat);
                        str_lng_array.add(startLocation_lng);
                    }

                }




                /** 處理html 文字 */
                if (i == 1) {
                    html_str_array = new ArrayList<String>();
                    List<HashMap<String, String>> html_list = result.get(i);
                    System.out.println(" main_html.toString():"
                            + html_list.toString());

                    for (int k = 0; k < html_list.size(); k++) {
                        HashMap<String, String> html_map = html_list.get(k);
                        String html_str = html_map.get("html_str");
                        html_str =html_str.replaceAll("\\<.*?\\>", "");
                        html_str_array.add(html_str);
                        Log.d(TAG,"路線資訊"+html_str_array.toString());
                    }

                }

                if (i == 3) {
                    ArrayList<String> time_array = new ArrayList<>();
                    List<HashMap<String, String>> dis_list = result.get(i);
                    System.out.println(" main_html.toString():"
                            + dis_list.toString());
                    for (int k = 0; k < dis_list.size(); k++) {
                        HashMap<String, String> html_map = dis_list.get(k);
                        String html_str = html_map.get("text");
                        Log.d(TAG,"distance_time: "+html_str);
                    }

                }
                if (i ==4){
                    ArrayList<String> html_str2_array = new ArrayList<String>();
                    List<HashMap<String, String>> html_list2 = result.get(i);
                    System.out.println(" main_html.toString():"
                            + html_list2.toString());

                    for (int k = 0; k < html_list2.size(); k++) {
                        HashMap<String, String> html_map = html_list2.get(k);
                        String html_str = html_map.get("detail");
                        if (html_str !=null){

                            Log.d(TAG,"路線資訊1"+html_str);

                            html_str =html_str.replaceAll("\\<.*?\\>", "");
                            Log.d(TAG,"路線資訊2"+html_str);
                        }

//                        html_str2_array.add(html_str);
//                        Log.d(TAG,"路線資訊"+html_str2_array.toString());
                    }

                }
            }
            // Adding all the points in the route to LineOptions

            lineOptions.addAll(points);
            lineOptions.width(10); // 導航路徑寬度
            lineOptions.color(Color.BLUE); // 導航路徑顏色
            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            //html
//            for (int i=1;i<st_lat_array.size();i++){
//                MarkerOptions options = new MarkerOptions();
//                LatLng ONE=new LatLng(Double.valueOf(st_lat_array.get(i)),Double.valueOf(str_lng_array.get(i)));
//                options.position(ONE)
//                        .title(html_str_array.get(i));
//
//                mMap.addMarker(options);
//
//            }






        }


    }
}