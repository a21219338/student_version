package com.example.student_version.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.student_version.Model.User;
import com.example.student_version.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddMemberAdapter extends RecyclerView.Adapter<AddMemberAdapter.ViewHolder> {
private Context mContext;
DatabaseReference reference;
private List<User> mUser;
private String id;

public AddMemberAdapter(Context mContext, List<User> mUser,String id) {
        this.mContext = mContext;
        this.mUser = mUser;
        this.id=id;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.new_member_item,parent,false);

        return  new AddMemberAdapter.ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = mUser.get(position);
        holder.username.setText(user.getusername());
        if(user.getImageUrl().equals("default")){
        holder.prfile_img.setImageResource(R.mipmap.ic_launcher);
        }else {
        Glide.with(mContext).load(user.getImageUrl()).into(holder.prfile_img);
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("creater",""+id);
        reference = FirebaseDatabase.getInstance().getReference("SelctMember");
        reference.child(id).setValue(hashMap).addOnSuccessListener(new OnSuccessListener<Void>(){

        @Override
        public void onSuccess(Void aVoid) {

        }
    }).addOnFailureListener(new OnFailureListener(){
        @Override
        public void onFailure(@NonNull Exception e) {
            Log.e("erreo",e.getMessage());
        }
    });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.inserts.setVisibility(View.VISIBLE);
                Log.d("USERsss",id);
                Log.d("OTHERsss",user.getId());
                HashMap<String, String> hashMaps = new HashMap<>();
                hashMaps.put("selectuserId", "" + user.getId());
                DatabaseReference rf=FirebaseDatabase.getInstance().getReference("SelctMember");
                rf.child(id).child("people").child(user.getId())
                        .setValue(hashMaps)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("current","cuccess");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("erreo",e.getMessage());
                    }
                });
           }
        });
}

@Override
public int getItemCount() {
        return mUser.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    public TextView username;
    public ImageView prfile_img,inserts;
    public ViewHolder(View itemView){
        super(itemView);
        username=itemView.findViewById(R.id.usernames);
        prfile_img=itemView.findViewById(R.id.profile_image);
        inserts=itemView.findViewById(R.id.inserts);

    }

}
}


