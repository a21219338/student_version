package com.example.student_version;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.student_version.database.DatabaseHelpler;
import com.example.student_version.Model.User;
import com.example.student_version.profile.ProfileActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    final static String TAG="MainActivity";
    private AppBarConfiguration mAppBarConfiguration;
    DatabaseHelpler mydb;
    TextView tv_username,tv_code;
    ImageView img;
    String NAMES="";
    FirebaseUser firebaseUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent =getIntent();

        firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
        String data=null;
        Log.d("CURRENT",TAG);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mydb=new DatabaseHelpler(this);
        Cursor cursor =mydb.getEventData();
        while (cursor.moveToNext()){
             data=cursor.getString(2);
        }
        if (data==null){
            Thread rd_lesson=new Thread(rd_lessonCheck); rd_lesson.start();
            Thread event=new Thread(rd_release);event.start();
            Thread maintext=new Thread(rd_mainteextbook);maintext.start();
            Thread exbook=new Thread(rd_exbook);exbook.start();
            Thread timelist=new Thread(td_time);timelist.start();
        }




            DrawerLayout drawer=findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            BottomNavigationView navView = findViewById(R.id.nav_bottom);
            BottomNavigationView navigationView1=findViewById(R.id.nav_messagebottom);
            BottomNavigationView navigationView2=findViewById(R.id.nav_mapbottom);

            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.navigation_bottom_exbook,R.id.navigation_bottom_release_content,R.id.navigation_bottom_todolist).build();

            AppBarConfiguration appBarConfiguration1=new AppBarConfiguration.Builder(
                    R.id.navigation_bottom_chat,R.id.navigation_bottom_group,R.id.navigation_bottom_friend).build();

        AppBarConfiguration  appBarConfiguration2=new AppBarConfiguration.Builder(
                R.id.navigation_bottom_road,R.id.navigation_bottom_street_view,R.id.navigation_bottom_direction).build();

            NavController navControllers= Navigation.findNavController(this,R.id.nav_host_fragment);
            NavigationUI.setupActionBarWithNavController(this, navControllers, appBarConfiguration);
            NavigationUI.setupActionBarWithNavController(this, navControllers, appBarConfiguration1);
        NavigationUI.setupActionBarWithNavController(this, navControllers, appBarConfiguration2);
            NavigationUI.setupWithNavController(navView, navControllers);
        NavigationUI.setupWithNavController(navigationView1, navControllers);
        NavigationUI.setupWithNavController(navigationView2, navControllers);
            mAppBarConfiguration=new AppBarConfiguration.Builder(
                    R.id.navigation_home,R.id.navigation_subject,R.id.navigation_callback,R.id.navigation_notice,R.id.navigation_map
                    ,R.id.navigation_safety,R.id.navigation_course
            ).setDrawerLayout(drawer).build();
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(navigationView, navController);

            navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
                @Override
                public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {

                    if (destination.getId() ==R.id.navigation_bottom_exbook ||destination.getId()  ==R.id.navigation_bottom_exbookDetail
                    || destination.getId()  == R.id.navigation_bottom_exbookPlaceDetail || destination.getId() ==R.id.navigation_bottom_release_content || destination.getId() ==R.id.navigation_bottom_release_content
                    || destination.getId()  == R.id.navigation_bottom_todolist){
                        navView.setVisibility(View.VISIBLE);
                    }else {
                        navView.setVisibility(View.GONE);
                    }

                    if (destination.getId() == R.id.navigation_notice || destination.getId() == R.id.navigation_bottom_group ||destination.getId() == R.id.navigation_bottom_chat
                   || destination.getId() == R.id.navigation_bottom_friend){
                        navigationView1.setVisibility(View.VISIBLE);
                    }else {
                        navigationView1.setVisibility(View.GONE);
                    }
                    if ( destination.getId() ==  R.id.navigation_bottom_street_view || destination.getId() == R.id.navigation_bottom_road || destination.getId() == R.id.navigation_bottom_direction){
                        navigationView2.setVisibility(View.VISIBLE);
                    }else{
                        navigationView2.setVisibility(View.GONE);
                    }
                }
            });

        if (intent.getStringExtra("destination") != null){
            navController.navigate(R.id.action_navigation_notice_to_navigation_bottom_group);

        }

            View header=navigationView.getHeaderView(0);
            img=header.findViewById(R.id.imageView);
            tv_code=header.findViewById(R.id.textView);
            tv_username =header.findViewById(R.id.nav_username);
            loadUserInfo();




        

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MainActivity.this, ProfileActivity.class);
                intent.putExtra("name",NAMES);
                startActivity(intent);

            }
        });




    }

    private void loadUserInfo() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Student");
        reference.child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user=snapshot.getValue(User.class);

                String usern=user.getusername();
                Log.d("user",usern);
                tv_username.setText(usern);
                NAMES=usern;
                tv_code.setText(user.getEmail());
                if (user.getImageUrl().equals("default")){
                    img.setImageResource(R.mipmap.ic_launcher);
                }else{
                    Glide.with(MainActivity.this).load(user.getImageUrl()).into(img);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    private  Runnable rd_lessonCheck=new Runnable() {
        @Override
        public void run() {
            try {
                URL url=new URL("http://coding.im.nuu.edu.tw/users/U0733138/userAccount/GetLessonCheck.php");
                HttpURLConnection connection=(HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(true);
                connection.connect();
                int response=connection.getResponseCode();
                if(response==HttpURLConnection.HTTP_OK){
                    InputStream is=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(is,"utf-8"),8);
                    String line = null;
                    while((line=br.readLine())!=null){
                        JSONArray jsonArray=new JSONArray(line);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject info=jsonArray.getJSONObject(i);
                            String eventName=info.getString("eventName");
                            String userName=info.getString("userName");
                            String subject=info.getString("subject");
                            String grade=info.getString("grade");
                            String version=info.getString("version");
                            String semester=info.getString("semester");
                            String chapter=info.getString("chapter");
                            String chapterName=info.getString("chapter_name");
                            String place=info.getString("place");
                            String location=info.getString("location");
                            String goal=info.getString("classGoal");
                            String time_dis=info.getString("timeDistribution");
                            String activity=info.getString("activityPlan");
                           mydb.insertLessonCheckData(eventName,userName,subject,grade,version,chapter,chapterName,place,location,goal,activity,time_dis,semester);
                        }
                    }

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private Runnable rd_release=new Runnable(){
        @Override
        public void run() {
            try{
                URL url=new URL("http://coding.im.nuu.edu.tw/users/U0733138/userAccount/getRelease_lesson.php");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setUseCaches(false);
                connection.connect();

                int responseCode = connection.getResponseCode();
                Log.d(TAG+"res", String.valueOf(responseCode));
                if(responseCode ==HttpURLConnection.HTTP_OK){
                    InputStream is=connection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                    String line=null;
                    while ((line =bufferedReader.readLine())!=null){
                        JSONArray data=new JSONArray(line);
                        for(int i=0;i<data.length();i++){
                            JSONObject jsonObject=data.getJSONObject(i);
                            String account=jsonObject.getString("account");
                            String classs=jsonObject.getString("class");
                            String eventName=jsonObject.getString("eventName");


                            mydb.insertEventData(account,classs,eventName);



                        }

                    }

                }


            }catch(Exception e){
                e.printStackTrace();
            }
        }
    };

    private  Runnable rd_mainteextbook=new  Runnable(){
        @Override
        public void run() {
            try{
                URL url=new URL("http://coding.im.nuu.edu.tw/users/U0733138/userAccount/getexbook.php");
                HttpURLConnection connection=(HttpURLConnection)url.openConnection();
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestMethod("POST");
                connection.setUseCaches(false);
                connection.connect();
                int response = connection.getResponseCode();
                if(response == HttpURLConnection.HTTP_OK){
                    InputStream inputStream=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                    String line=null;
                    while((line=br.readLine()) != null){
                        JSONArray jsonArray=new JSONArray(line);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            String id1=jsonObject.getString("id1");String version=jsonObject.getString("version") ;String clp =jsonObject.getString("clp");
                            String subject=jsonObject.getString("subject");String grade=jsonObject.getString("grade") ;String semester =jsonObject.getString("semester");
                            String chapter =jsonObject.getString("chapter");String unit=jsonObject.getString("unit") ;String chaptername =jsonObject.getString("chapter_name");
                            String area =jsonObject.getString("area");String place=jsonObject.getString("place") ;String nickname =jsonObject.getString("nickname");
                            String mon =jsonObject.getString("mon");String tues=jsonObject.getString("tues") ;String wed =jsonObject.getString("wed");
                            String thur =jsonObject.getString("thur");String fri=jsonObject.getString("fri") ;String sat =jsonObject.getString("sat");
                            String sun  =jsonObject.getString("sun");String  season=jsonObject.getString("season") ;String location =jsonObject.getString("location");
                            String introduction=jsonObject.getString("introduction");String remarks=jsonObject.getString("remarks") ;String sort  =jsonObject.getString("sort");
                            String exbook =jsonObject.getString("exbook");String addexbook=jsonObject.getString("addexbook");
                            mydb.insertMainTextData(id1,version, clp , subject , grade, semester,chapter , unit,  chaptername , area , place , nickname,  mon , tues , wed ,
                                    thur , fri , sat , sun , season , location , introduction ,  remarks , sort , exbook , addexbook );




                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private  Runnable rd_exbook=new Runnable(){
        @Override
        public void run() {
            try{
                URL url=new URL("http://coding.im.nuu.edu.tw/users/U0733138/userAccount/getAddExbook.php");
                HttpURLConnection connection=(HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.connect();
                int response=connection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK){
                    InputStream inputStream=connection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                    String line=null;
                    while ((line =br.readLine())!=null){
                        JSONArray json=new JSONArray(line);
                        for(int i=0;i<json.length();i++){
                            JSONObject jsonObject=json.getJSONObject(i);
                            String clps=jsonObject.getString("clp") ,  subjects= jsonObject.getString("subject"), areas=jsonObject.getString("area");
                            String places=jsonObject.getString("place") ,nicknames=jsonObject.getString("nickname"),  mons=jsonObject.getString("mon");
                            String tuess=jsonObject.getString("tues") , weds =jsonObject.getString("wed") ,thurs=jsonObject.getString("thur");
                            String fris=jsonObject.getString("fri") , sats =jsonObject.getString("sat"), suns=jsonObject.getString("sun") ;
                            String seasons =jsonObject.getString("season"),locations=jsonObject.getString("location") ,
                                    introductions =jsonObject.getString("introduction"), remarkss=jsonObject.getString("remarks");
                            mydb.insertExbookData(clps,subjects,areas,places,nicknames,mons,tuess,weds,thurs,fris,sats,suns,seasons,locations,introductions,remarkss);
                        }
                    }
                }

            }catch(Exception e){
                e.printStackTrace();
            }
        }
    };

    private  Runnable td_time=new Runnable(){
        @Override
        public void run() {
            try{
                URL url=new URL("http://coding.im.nuu.edu.tw/users/U0733138/userAccount/getTime.php");
                HttpURLConnection connection= (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(true);
                connection.connect();
                int response=connection.getResponseCode();
                if (response ==HttpURLConnection.HTTP_OK){
                    InputStream inputStream=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                    String line=null;
                    while ((line = br.readLine()) !=null){
                        JSONArray jsonArray=new JSONArray(line);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            String id=jsonObject.getString("id");
                            String time=jsonObject.getString("time");
                            mydb.inserTimetData(id,time);


                        }
                    }
                }




            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };
    private Runnable rd_studentdata=new Runnable(){
        @Override
        public void run() {
            try{
                URL url =new URL("http://coding.im.nuu.edu.tw/users/U0733138/userAccount/getDatas.php");
                HttpURLConnection connection= (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(true);
                connection.connect();
                int response=connection.getResponseCode();
                if (response ==HttpURLConnection.HTTP_OK){
                    InputStream inputStream=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(inputStream,"utf-8"),8);
                    String line=null;
                    while ((line = br.readLine()) !=null){
                        JSONArray jsonArray=new JSONArray(line);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            String school =jsonObject.getString("school");
                            String county=jsonObject.getString("county");
                            String semester	 =jsonObject.getString("semester");
                            String classes=jsonObject.getString("class");
                            String student_name =jsonObject.getString("student_name");
                            String gender=jsonObject.getString("gender");
                            String student_ID =jsonObject.getString("student_ID");
                            String ID_number=jsonObject.getString("ID_number");
                            String guardian =jsonObject.getString("guardian");
                            String guardian_phone=jsonObject.getString("guardian_phone");
                            String student_phone =jsonObject.getString("student_phone");




                        }
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this,Login.class));
                finish();
                return true;
        }
        return  false;
    }



}