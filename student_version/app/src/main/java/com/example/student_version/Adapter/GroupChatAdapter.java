package com.example.student_version.Adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.student_version.Model.GroupChat;
import com.example.student_version.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.List;

public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder> {
    public static final  int MSG_TYPE_LEFT=0;
    public static final  int MSG_TYPE_RIGHT=1;
    private Context mContext;
    private List<GroupChat> mGroupChat;
    private  String imageUrl;
    FirebaseAuth firebaseAuth;
    FirebaseUser fuser;
    public GroupChatAdapter(Context mContext, List<GroupChat> mGroupChat) {
        this.mContext = mContext;
        this.mGroupChat = mGroupChat;

        firebaseAuth = FirebaseAuth.getInstance();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == MSG_TYPE_RIGHT){
            View view = LayoutInflater.from(mContext).inflate(R.layout.groupchat_right,parent,false);
            return  new GroupChatAdapter.ViewHolder(view);
        }else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.groupchat_left,parent,false);
            return  new GroupChatAdapter.ViewHolder(view);
        }

    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GroupChat groupChatts=mGroupChat.get(position);
        String timestampss =groupChatts.getTimestamp();
        String message =groupChatts.getMessage();
        String senderUid=groupChatts.getSender();
        String messageImg=groupChatts.getType();
        Log.d("type",messageImg);
        Calendar c=Calendar.getInstance();
        c.setTimeInMillis(Long.parseLong(timestampss));

        String dateTime="";
        if (Calendar.AM == c.get(Calendar.AM_PM)){
            dateTime =""+ c.get(Calendar.HOUR)+ ":"+c.get(Calendar.MINUTE)+" am" ;
        }else{
            dateTime =""+ c.get(Calendar.HOUR)+ ":"+c.get(Calendar.MINUTE)+" pm" ;
        }
        if (messageImg.equals("text")){
            holder.message_Img.setVisibility(View.GONE);
            holder.show_message.setVisibility(View.VISIBLE);
            holder.show_message.setText(message);
        }else {
            holder.message_Img.setVisibility(View.VISIBLE);
            holder.show_message.setVisibility(View.GONE);
            if (message.equals("")){
                holder.message_Img.setImageResource(R.mipmap.ic_launcher);
            }else {
                Glide.with(mContext).load(message).into(holder.message_Img);
            }
        }

        holder.show_time.setText(dateTime);

        setUserUrls(groupChatts,holder);

    }

    private void setUserUrls(GroupChat groupChatts, ViewHolder holder) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.orderByChild("uid").equalTo(groupChatts.getSender())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for(DataSnapshot dataSnapshot :snapshot.getChildren()){
                            imageUrl =""+dataSnapshot.child("imageUrl").getValue();


                            if (imageUrl.equals("")){
                                holder.prfile_img.setImageResource(R.mipmap.ic_launcher);
                            }else {
                                Glide.with(mContext).load(imageUrl).into(holder.prfile_img);
                            }                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    @Override
    public int getItemCount() {
        return mGroupChat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView show_message;
        public ImageView prfile_img,message_Img;
        public TextView show_time;
        public ViewHolder(View itemView){
            super(itemView);
            show_message=itemView.findViewById(R.id.show_message);
            prfile_img=itemView.findViewById(R.id.profile_image);
            show_time=itemView.findViewById(R.id.time);
            message_Img=itemView.findViewById(R.id.iv_messageImg);



        }
    }

    @Override
    public int getItemViewType(int position) {

        if (mGroupChat.get(position).getSender().equals(firebaseAuth.getUid())){
            return MSG_TYPE_RIGHT;
        }else {
            return MSG_TYPE_LEFT;
        }
    }
}


