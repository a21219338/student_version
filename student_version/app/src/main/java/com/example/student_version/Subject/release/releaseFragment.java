package com.example.student_version.Subject.release;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.student_version.R;
import com.example.student_version.Subject.ScheduleActivity;
import com.example.student_version.database.DatabaseHelpler;

public class releaseFragment extends Fragment {
    String events="";
    TextView tv_title,tv_subject,tv_place,tv_goal;
    ImageView scehule;
    DatabaseHelpler mydb;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_release,container, false);
        mydb=new DatabaseHelpler(getActivity());
        mydb.deleteSearchData();
        Bundle bundle = getArguments();
        if(bundle != null)
        {
           events = bundle.getString("event");
        }
//        mydb.inserSearchData(events);
        Log.d("eve",events);
        tv_title=root.findViewById(R.id.event);
        tv_subject=root.findViewById(R.id.subjects);
        tv_title.setText(events);
        scehule=root.findViewById(R.id.scehule);
        scehule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ScheduleActivity.class));
            }
        });

        tv_place=root.findViewById(R.id.place);
        tv_goal=root.findViewById(R.id.goal);
        
        Cursor cursor=mydb.selectLessonData(events);
        while(cursor.moveToNext()){
            tv_subject.setText(cursor.getString(3));

            tv_place.setText(Html.fromHtml(cursor.getString(9)));

            tv_goal.setText(cursor.getString(11));

        }
        Log.d("N", String.valueOf(tv_goal.getText()));
        mydb.inserSearchData(events);
        return root;
    }
}
