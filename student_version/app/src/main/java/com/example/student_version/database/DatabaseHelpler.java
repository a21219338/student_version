package com.example.student_version.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelpler extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="student.db";
    public static final String eventTable ="eventTable";
    public static final String MaintextTable="maintextTable";
    public static final String TimeListTable="TimeListTable";
    public static final String ExbookTable="ExbookTable";
    public static final String SearchTable="SearchTable";
    public static final String LessonCheckTable="LessonCheckTable";

    String col_account="ACCOUNT",col_class="CLASS",col_eventName="EVENT"; String col_TIME="TIME";
    String col_id1="ID_1";String col_version="VERSION", col_clp="CLP", col_subject="SUBJECT";
    String col_grade="GRADE",col_semester="SEMESTER",col_chapter="CHAPTER",col_unit="UNIT",
            col_chaptername="CHAPTER_NAME",col_area="AREA", col_place="PLACE",col_nickname="NICKNAME",
            col_mon="MON",col_tues="TUES", col_wed="WED",col_thur="THUR",col_fri="FRI",col_sat="SAT",
            col_sun="SUN", col_season="SEASON",col_location="LOCATION",col_introduction="INTRODUCTION",
            col_remarks="REMARKS",col_sort="SORT",col_exbook="EXBOOK",col_addexbook="ADDEXBOOK";
    String col_goal="GOAL",col_timeDistribution="TIMEDISTRIBUTION", col_ACTIVITY="ACTIVITY";

    public DatabaseHelpler(@Nullable Context context) { super(context,DATABASE_NAME,null,2); }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table "+LessonCheckTable+" (ID INTEGER PRIMARY KEY AUTOINCREMENT,EVENT TEXT,ACCOUNT TEXT,SUBJECT TEXT,SEMESTER TEXT,"+
                "GRADE TEXT,VERSION TEXT, CHAPTER TEXT,CHAPTER_NAME TEXT, PLACE TEXT, LOCATION TEXT,GOAL TEXT,TIMEDISTRIBUTION TEXT,ACTIVITY TEXT)");

        db.execSQL(" create  table  "+eventTable+" ( ID INTEGER PRIMARY KEY AUTOINCREMENT,ACCOUNT TEXT,CLASS TEXT,EVENT TEXT)");

        db.execSQL(" create table " +TimeListTable+"( ID INTEGER PRIMARY KEY AUTOINCREMENT,ID_1 TEXT,TIME TEXT)" );

        db.execSQL(" create  table  "+MaintextTable+" ( ID INTEGER PRIMARY KEY AUTOINCREMENT,ID_1 TEXT,VERSION TEXT,CLP TEXT," +
                "SUBJECT TEXT,GRADE TEXT,SEMESTER TEXT,CHAPTER TEXT,UNIT TEXT,CHAPTER_NAME TEXT,AREA TEXT,PLACE TEXT,NICKNAME TEXT," +
                "MON TEXT,TUES TEXT,WED TEXT,THUR TEXT,FRI TEXT,SAT TEXT,SUN TEXT,SEASON TEXT,LOCATION TEXT,INTRODUCTION TEXT," +
                "REMARKS TEXT,SORT TEXT,EXBOOK TEXT,ADDEXBOOK TEXT)");

        db.execSQL(" create  table  "+ExbookTable+" ( ID INTEGER PRIMARY KEY AUTOINCREMENT,CLP TEXT," +
                "SUBJECT TEXT,AREA TEXT,PLACE TEXT,NICKNAME TEXT," +
                "MON TEXT,TUES TEXT,WED TEXT,THUR TEXT,FRI TEXT,SAT TEXT,SUN TEXT,SEASON TEXT,LOCATION TEXT,INTRODUCTION TEXT," +
                "REMARKS TEXT)");
        db.execSQL("create table "+SearchTable+"(ID INTEGER PRIMARY KEY AUTOINCREMENT,EVENT TEXT)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+eventTable);
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS "+TimeListTable);
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS "+MaintextTable);
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS "+SearchTable);
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS "+ExbookTable);
    }
    public void delete(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("delete from "+eventTable);
        db.execSQL("delete from "+LessonCheckTable);
        db.execSQL("delete from "+MaintextTable);
        db.execSQL("delete from "+ExbookTable);
        db.execSQL(" delete from "+TimeListTable);
        db.execSQL(" delete from "+SearchTable);

    }
    //event table
    public boolean insertEventData(String account,String classs,String name){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from "+eventTable,null);
        boolean isduplicate=false;

        while (cursor.moveToNext()){
            if( name.equals( cursor.getString(3))){
                isduplicate=true;
                break;
            }
        }
        if( isduplicate == false){
            ContentValues values=new ContentValues();
            values.put(col_account,account);values.put(col_class,classs);
            values.put(col_class,name);

            long success=db.insertWithOnConflict(eventTable,null,values,SQLiteDatabase.CONFLICT_REPLACE);
            if(success ==-1)return false;
            else return true;
        }else{
            return false;
        }

    }
    public Cursor getEventData(){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from "+eventTable,null);
        return cursor;
    }
    public  boolean insertLessonCheckData(String event,String account,String subject,String grade ,String version ,String chapter,String chaptername,String place,String location,String goal,String activity,String time_distribution,String semester){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select * from "+LessonCheckTable,null);
        boolean isduplicate=false;
        while (cursor.moveToNext()){
            if(event.equals(cursor.getString(3))){
                isduplicate=true;
                break;
            }
        }if(isduplicate ==false){
            ContentValues values=new ContentValues();
            values.put(col_chaptername,chaptername);
            values.put(col_eventName,event);values.put(col_account,account);values.put(col_subject,subject);values.put(col_grade,grade);
            values.put(col_version,version);values.put(col_chapter,chapter);values.put(col_place,place);values.put(col_goal,goal);
            values.put(col_location,location);values.put(col_ACTIVITY,activity);values.put(col_timeDistribution,time_distribution);values.put(col_semester,semester);
            long success=db.insertWithOnConflict(LessonCheckTable,null,values,SQLiteDatabase.CONFLICT_REPLACE);
            if(success ==-1)return false;
            else return true;
        }else{
            return false;
        }
    }
    public Cursor getLessonData(){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from "+LessonCheckTable,null);
        return cursor;
    }
    public Cursor selectLessonData(String event){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select * from "+LessonCheckTable+" WHERE EVENT =? " ,new String[] {event});

        return cursor;
    }

    public boolean insertMainTextData(String id1,String version ,String clp , String subject ,String grade,String semester,String chapter ,
                                      String unit, String chaptername ,String area ,String place ,String nickname, String mon ,String tues ,String wed ,
                                      String thur ,String fri ,String sat ,String sun ,String season ,String location ,String introduction , String remarks ,String sort ,String exbook ,String addexbook ){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select * from "+MaintextTable,null);
        boolean isRepeat=false;
        while(cursor.moveToNext()){
            if(cursor.getString(9).equals(chaptername) && cursor.getString(11).equals(place) &&cursor.getString(22).equals(introduction)){
                isRepeat=true;
                break;
            }
        }
        if(isRepeat == false){
            ContentValues values=new ContentValues();
            values.put(col_id1,id1);values.put(col_version,version); values.put(col_clp,clp);values.put(col_subject,subject); values.put(col_grade,grade);values.put(col_semester,semester);
            values.put(col_chapter,chapter);values.put(col_unit,unit); values.put(col_chaptername,chaptername);values.put(col_area,area); values.put(col_place,place);values.put(col_nickname,nickname);
            values.put(col_mon,mon);values.put(col_tues,tues); values.put(col_wed,wed);values.put(col_thur,thur); values.put(col_fri,fri);values.put(col_sat,sat);
            values.put(col_sun,sun);values.put(col_season,season); values.put(col_location,location);values.put(col_introduction,introduction); values.put(col_remarks,remarks);values.put(col_sort,sort);
            values.put(col_exbook,exbook);values.put(col_addexbook,addexbook);
            long success=db.insertWithOnConflict(MaintextTable,null,values,SQLiteDatabase.CONFLICT_REPLACE);
            if(success ==-1)return false;
            else return true;
        }return false;


    }

    public boolean insertExbookData(String clps , String subjects ,String areas ,String places ,String nicknames, String mons ,String tuess ,String weds ,
                                    String thurs ,String fris ,String sats ,String suns ,String seasons ,String locations ,String introductions , String remarkss ){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select* from "+ExbookTable,null);
        boolean isRepeat=false;
        while(cursor.moveToNext()){
            if(places.equals(cursor.getString(4)) && introductions.equals(cursor.getString(15))){
                isRepeat=true;
                break;
            }
        }
        if (isRepeat == false){
            ContentValues values=new ContentValues();
            values.put(col_clp,clps);values.put(col_subject,subjects);
            values.put(col_area,areas); values.put(col_place,places);values.put(col_nickname,nicknames);
            values.put(col_mon,mons);values.put(col_tues,tuess); values.put(col_wed,weds);values.put(col_thur,thurs); values.put(col_fri,fris);values.put(col_sat,sats);
            values.put(col_sun,suns);values.put(col_season,seasons); values.put(col_location,locations);values.put(col_introduction,introductions); values.put(col_remarks,remarkss);
            long success=db.insertWithOnConflict(ExbookTable,null,values,SQLiteDatabase.CONFLICT_REPLACE);
            if(success ==-1)return false;
            else return true;
        }return false;
    }

    public boolean inserTimetData(String id1,String time){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from "+TimeListTable,null);
        boolean isRepeat=false;
        while (cursor.moveToNext()){
            if(time.equals(cursor.getString(1))){
                isRepeat=true;
                break;
            }
        }
        if (isRepeat == false) {
            ContentValues values=new ContentValues();
            values.put(col_id1,id1);
            values.put(col_TIME,time);
            long success=db.insertWithOnConflict(TimeListTable,null,values,SQLiteDatabase.CONFLICT_REPLACE);
            if(success ==-1)return false;
            else return true;
        }return false;

    }
    public Cursor selectTimeListData(String id){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select * from "+TimeListTable+" WHERE ID_1 =? " ,new String[] {id});

        return cursor;
    }
    public boolean inserSearchData(String event){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from "+SearchTable,null);
        boolean isRepeat=false;
        while (cursor.moveToNext()){
            if(event.equals(cursor.getString(1))){
                isRepeat=true;
                break;
            }
        }
        if (isRepeat == false) {
            ContentValues values=new ContentValues();
            values.put(col_eventName,event);
            long success=db.insertWithOnConflict(SearchTable,null,values,SQLiteDatabase.CONFLICT_REPLACE);
            if(success ==-1)return false;
            else return true;
        }return false;

    }
    public Cursor getSearchData(){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery(" select * from "+SearchTable,null);
        return cursor;
    }
    public Cursor selectMainTextData(String place){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select * from "+MaintextTable+" WHERE PLACE =? " ,new String[] {place});

        return cursor;
    }
    public Cursor selectMainTextId(String id){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(" select * from "+MaintextTable+" WHERE ID_1 =? " ,new String[] {id});

        return cursor;
    }
    public void deleteSearchData(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL(" delete from "+SearchTable);

    }






}
