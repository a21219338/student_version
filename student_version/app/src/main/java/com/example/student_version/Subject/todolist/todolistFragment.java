package com.example.student_version.Subject.todolist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.student_version.Adapter.ToDolistAdapter;
import com.example.student_version.R;

import java.util.ArrayList;
import java.util.List;

public class todolistFragment extends Fragment {
    private ToDolistAdapter toDolistAdapter;
    private List<String> data;
    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_todolist,container, false);
        RecyclerView recyclerView=root.findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true); //新增item 不影響recycle view尺寸
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        data = new ArrayList<>();
        data.add("健保卡");
        data.add("水壺");
        data.add("雨衣");
        data.add("輕便雨衣");
        toDolistAdapter =new ToDolistAdapter(getActivity(),data);
        recyclerView.setAdapter(toDolistAdapter);
        return root;
    }
}
