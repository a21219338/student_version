package com.example.student_version.Model;

public class PlaceData {
    private String place;
    private Integer img;

    public PlaceData(String place, Integer img) {
        this.place = place;
        this.img = img;
    }

    public PlaceData() {
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Integer getImg() {
        return img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }
}
