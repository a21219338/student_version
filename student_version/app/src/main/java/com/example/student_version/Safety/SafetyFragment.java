package com.example.student_version.Safety;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.student_version.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class SafetyFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "SafetyFragment";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private LocationManager locationManager;
    private LatLng mylocation;
    private ArrayList<LatLng> origin;
    private WifiManager wifiManager;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private GoogleMap mMap;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_safety,container,false);
        getPermission();
        return root;
    }
    private void getPermission() {
        String[] permissions = {FINE_LOCATION, COURSE_LOCATION}; //自訂權限
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(getActivity().WIFI_SERVICE);

        boolean LOCATION_setting = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER); //用來確認手機定位服務是否可使用
        if (LOCATION_setting) {
            //權限是否打開
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), COURSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "失敗");
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_REQUEST_CODE);
            } else {
                Log.e(TAG, "成功");
                init();

            }

        } else {
            Log.e(TAG, "未開啟定位");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        if (!wifiManager.isWifiEnabled()) {
            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            startActivity(intent);
        }
    }
    private void init() {
        Log.d(TAG, "init:地圖初始化");
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);  //地圖初始化

    }



    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        getDeviceLocation();
    }
    private void getDeviceLocation() {
        Log.d(TAG,"getDeviceLocation:正在取得定位資訊");
        fusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(getActivity());
        try{
            //task 是在背景執行的一種非同步
            Task location=fusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){ //找到最新的定位資訊
                        Log.d(TAG,"OnCompleted:found location");
                        Location current_location=(Location) task.getResult(); //取得結果
                        mylocation=new LatLng(current_location.getLatitude(),current_location.getLongitude());
                        Log.d(TAG,"LAT:"+current_location);
                        if(current_location !=null){
                            moveCamera(new LatLng(current_location.getLatitude(),current_location.getLongitude()),
                                    DEFAULT_ZOOM,"my location");

                        }
                    }else {
                        Log.d(TAG,"current location is null");
                        Toast.makeText(getContext(),"unable to get current location ",Toast.LENGTH_SHORT).show();

                    }
                }
            });


        }catch (SecurityException e){
            Log.e(TAG,"getDevicesSecurityException"+e.getMessage());

        }

    }
    private void moveCamera(LatLng latLng, float Zoom, String title) {
        Log.d(TAG,"moveCameraTo"+latLng);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,Zoom));
        MarkerOptions options=new MarkerOptions()
                .position(latLng)
                .title(title);
        mMap.addMarker(options);

        if (title.equals("my location")){
            origin=new ArrayList<LatLng>();
            origin.add(latLng);
            Log.d(TAG,"origin:"+origin.toString());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to Mountain View
                    .zoom(20)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(30)// Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }


}
